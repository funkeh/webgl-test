/*global require*/
/*jslint sloppy: true*/
// chikun :: 2014
// Loads libraries


// List of third-party libraries to require
var requireList1 = [
    "src/lib/jquery-1.11.1.min.js",
    "src/lib/hand.minified-1.2.js",
    "src/lib/cannon.js",
    "src/lib/Oimo.js",
    "src/lib/babylon.1.13.js"
];
// Personal libraries
var requireList2 = [
    "input",
    "maths",
    "mouse"
];
// Object-specific code
var requireList3 = [
];


// Actually load the libraries synchronously
require(requireList1, function () {
    require(requireList2, function () {
        require(requireList3, function () {
            require(["main"]);
        });
    });
});
