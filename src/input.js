/*global THREE*/
/*jslint sloppy: true*/
// chikun :: 2014
// Handles all input

var input, pressed, key;


// Contains all input functions
input = { };

// Contains currently pressed keys
pressed = { };

// Table of keys
key = {
    moveL:  65,
    moveR:  68,
    moveU:  87,
    moveD:  83,
    moveVU: 16,
    moveVD: 17,
    jump:   32
};


// Run at game initialisation
input.init = function () {

    // Set up input handlers
    document.body.onkeydown = function (e) {
        e = e || window.event;
        document.title = document.title + " Keypress: " + e.keyCode;
        pressed[e.keyCode] = true;
    };
    document.body.onkeyup = function (e) {
        e = e || window.event;
        delete pressed[e.keyCode];
    };
    document.body.onblur = function () {
        pressed = { };
    };

};


// Check if input is pressed
input.check = function (num) {

    return (pressed[num] || false);

};
