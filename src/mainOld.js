/*jslint plusplus: true, sloppy: true, vars: true*/
/*global THREE,mdl,mat,requestAnimationFrame,alert,input,key,pressed,
mouse*/
// chikun :: 2014
// Main game loop


var scene, camera, renderer, rot, controls;
var geometry, material, mesh, blocks, player, fog;

// Object which holds our game functions
var game = { };

// Used for dt updates
var lastTime = (new Date()).getTime();

// Variables which allow us to use screen size
var screenWidth  = window.innerWidth;
var screenHeight = window.innerHeight;


// Creates a new block and adds it to the world
function addBlock(model, material, x, y, z) {

    // Create mesh for future use
    var mesh = new THREE.Mesh(model, material);

    // Position mesh in the game world
    mesh.position.x = x;
    mesh.position.y = y;
    mesh.position.z = z;

    // Make sure you can collide with this
    mesh.helper = new THREE.BoundingBoxHelper(mesh, 0xff0000);

    // Add mesh to blocks table
    blocks.push(mesh);

    // Add mesh to scene
    scene.add(blocks[blocks.length - 1]);

}

// Performed on game startup
game.load = function () {

    var x, y, z;

    // Initialise the input hooks
    input.init();

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(80, screenWidth / screenHeight,
        1, 10000);
    camera.position.y = 30;
    camera.position.z = 0;
    camera.rotation.order = "YXZ";

    player = new THREE.Mesh(mdl.player, mat.wireframe);
    player.material.visible = false;
    player.position.y = 30;
    scene.add(player);
    player.add(camera);

    var dLight = new THREE.DirectionalLight(0xffffff, 0.6);
    dLight.position.set(-1, 1, -0.75);
    //scene.add(dLight);

    var aLight = new THREE.AmbientLight(0x333333);
    scene.add(aLight);

    blocks = [];

    for (x = 0; x < 20; x++) {
        for (y = 0; y < 20; y++) {
            for (z = 0; z < 8; z++) {
                addBlock(mdl.cube, mat.crate,
                    25 + (50 * x), 25 + (y * 50), 975 - (z * 75));
            }
        }
    }
    addBlock(mdl.floor, mat.grass,
        500, -10, 500);

    var flashlight = new THREE.SpotLight(0xffffff, 1, 400);
    camera.add(flashlight);
    flashlight.position.set(0, 0, 1);
    flashlight.castShadow = true;
    flashlight.target = camera;

    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(screenWidth, screenHeight);

    document.body.appendChild(renderer.domElement);

};

game.step = function () {

    // Run step on next draw step
    requestAnimationFrame(game.step);

    var time, dt, dir, viewY, viewZ, i;

    // Calculate delta time
    time =  (new Date()).getTime();
    dt =    (time - lastTime) / 1000;
    lastTime = time;

    // Draw the scene
    game.draw(dt);

};

game.draw = function (dt) {

    // If mouse is locked, then enable movement
    if (mouse.isLocked) {

        var xMove = 0, zMove = 0;

        // Move player
        if (input.check(key.moveL)) {
            xMove = -1;
        }
        if (input.check(key.moveR)) {
            xMove = 1;
        }
        if (input.check(key.moveU)) {
            zMove = -1;
        }
        if (input.check(key.moveD)) {
            zMove = 1;
        }

        player.translateX(256 * dt * xMove);
        /*
        while (Math.overlap(player, blocks) && xMove !== 0) {
            // What the fuck do i do here
            player.translateX(-xMove);
        }*/

        player.translateZ(256 * dt * zMove);

        /*while (Math.overlap(player, blocks) && zMove !== 0) {
            // What the fuck do i do here
            player.translateZ(-zMove);
        }*/
    }

    renderer.render(scene, camera);

};

game.resize = function () {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
};

game.load();
game.step();

// Add listener to resize event
window.addEventListener('resize', game.resize, false);
