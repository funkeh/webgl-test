/*jslint sloppy: true*/
/*global console,camera,player*/
// chikun :: 2014
// Mouse functions


// Contains all mouse functions.
var mouse = {
    isLocked: false
};


// Check if mouse locking is supported
mouse.canLock = function () {

    // Check if pointer lock is available
    var havePointerLock = document.hasOwnProperty('pointerLockElement') ||
        document.hasOwnProperty('mozPointerLockElement') ||
        document.hasOwnProperty('webkitPointerLockElement');

    // Return the value
    return (havePointerLock);
};


// Locks the mouse to the screen
mouse.lock = function () {

    // We're going to lock to the whole screen
    var lockElement = document.body;

    // Determine function based on browser
    lockElement.requestPointerLock = lockElement.requestPointerLock ||
        lockElement.mozRequestPointerLock ||
        lockElement.webkitRequestPointerLock;

    // Request that we lock the mouse
    lockElement.requestPointerLock();
};


// Unlocks the mouse from the screen
mouse.unlock = function () {

    // We're going to lock to the whole screen
    var lockElement = document.body;

    // Determine function based on browser
    lockElement.exitPointerLock = lockElement.exitPointerLock ||
        lockElement.mozExitPointerLock ||
        lockElement.webkitExitPointerLock;

    // Request that we unlock the mouse
    lockElement.exitPointerLock();
};


// Called on mouse movement
mouse.onMove = function (e) {

    // Gather movement based on browser
    var movementX = e.movementX || e.mozMovementX ||
        e.webkitMovementX || 0,
        movementY = e.movementY || e.mozMovementY ||
        e.webkitMovementY || 0;

    // Actually rotate the camera
    /*player.rotation.y -= movementX / 400;*/
    camera.rotation.x += movementY / 400;
    camera.rotation.y += movementX / 400;

    if (camera.rotation.x > 1.50) {
        camera.rotation.x = 1.50;
    } else if (camera.rotation.x < -1.50) {
        camera.rotation.x = -1.50;
    }
};


// Called on mouse lock state change
mouse.onLockChange = function (e) {

    // We're going to lock to the whole screen
    var lockElement = document.body;

    // Gather lock info based on browser
    if (document.pointerLockElement === lockElement ||
            document.mozPointerLockElement === lockElement ||
            document.webkitPointerLockElement === lockElement) {

        // Pointer was locked, enable listener
        document.addEventListener("mousemove", mouse.onMove, false);

        // Set locked variable to true
        mouse.isLocked = true;

    } else {

        // Pointer was unlocked, disable listener
        document.removeEventListener("mousemove", mouse.onMove, false);

        // Set locked variable to false
        mouse.isLocked = false;

    }
};


// Hook mouse lock state change function to listener
document.addEventListener("pointerlockchange",
                          mouse.onLockChange, false);
document.addEventListener("mozpointerlockchange",
                          mouse.onLockChange, false);
document.addEventListener("webkitpointerlockchange",
                          mouse.onLockChange, false);
