/*jslint plusplus: true, sloppy: true, vars: true*/
/*global BABYLON,input,getDeltaTime,mouse,key*/
// chikun :: 2014
// Main game loop


// Declare variables for future use
var canvas, engine, scene, camera, camSensor,
    prog, player, startPos, gamepads;

// Contains all game functions
var game = { };

// Used for delta time updates
var lastTime = (new Date()).getTime();

// Contains all gamepads
var gamepads = { };

// Resize game canvas to fit full screen
function canvasResize() {

    // Update canvas size based on window size
    canvas.width =  window.innerWidth;
    canvas.height = window.innerHeight;

    engine.resize();

}


// Performed on game load
game.load = function () {

    // Initialise the input hooks
    input.init();

    canvas =    document.getElementById("renderCanvas");
    engine =    new BABYLON.Engine(canvas, true);

    BABYLON.SceneLoader.Load("res/", "mapStart.babylon",
        engine, function (newScene) {
            // Wait for textures and shaders to be ready
            newScene.executeWhenReady(function () {

                // Remove loading div
                document.getElementById("loadScreen").innerHTML = "";

                startPos = newScene.activeCamera.position;

                camera = new BABYLON.FreeCamera("FreeCamera",
                    startPos, newScene);
                camera.fov = (75 * Math.PI / 180);
                camera.applyGravity = true;
                camera.checkCollisions = true;
                camera.ellipsoid = new BABYLON.Vector3(0.15, 0.4, 0.15);

                camera.keysUp = [ key.moveU ];
                camera.keysDown = [ key.moveD ];
                camera.keysLeft = [ key.moveL ];
                camera.keysRight = [ key.moveR ];
                camera.physics = 0.1;

                camera.cameraDirection.y = -0.4;

                // Increase draw distance
                camera.speed = 0.3;
                camera.minZ = 0.01;
                camera.maxZ = 10000;

                newScene.activeCamera = camera;
                newScene.activeCamera.attachControl(canvas);

                newScene.enablePhysics();

                newScene.collisionsEnabled = true;

                newScene.gravity = new BABYLON.Vector3(0, -0.2, 0);

                scene = newScene;

                // Game update loop
                engine.runRenderLoop(function () {
                    game.step();
                    newScene.render();
                });

                game.step();
            });
        }, function (progress) {
            // ADD LOADING BAR HERE
        });

    // Initial resize
    canvasResize();

    // Hook canvasResize onto window resize
    window.addEventListener("resize", canvasResize, false);
};


// Performed on game loop
game.step = function () {

    // Delta variables
    var dt = getDeltaTime();

    document.title = "Approx FPS: " + Math.round(BABYLON.Tools.GetFps());

    // If mouse is locked, then enable movement
    if (mouse.isLocked) {
        document.getElementById("loadScreen").innerHTML = camera.inertia;
        if (input.check(key.jump) && camera.cameraDirection.y === 0) {
            camera.cameraDirection.y = 1;
        }
    }

};


// If game engine is supported, will load the game. Otherwise show error.
if (BABYLON.Engine.isSupported()) {
    game.load();
} else {
    document.body.innerHTML = "This browser is not supported. Please " +
        "update to a newer browser such as Chrome or Firefox. If you " +
        "are running either of these, please upgrade your browser or " +
        "graphics drivers.";
}
