/*jslint plusplus: true, sloppy: true, vars: true*/
/*global lastTime*/
// chikun :: 2014
// Adds some mathematical functions


function getDeltaTime() {

    var dt, time;

    // Calculate delta time
    time =  (new Date()).getTime();
    dt =    (time - lastTime) / 1000;
    lastTime = time;

    // Return the delta time
    return (dt);

}
